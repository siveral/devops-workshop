package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     *
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression) {
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+", "");

        String result = "";

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */
        if (expressionTrimmed.matches("^\\d+(?:\\s*[+]\\s*\\d+)*$")) result = sum(expressionTrimmed);
        else if (expressionTrimmed.matches("^\\d+(?:\\s*[-]\\s*\\d+)*$")) result = subtraction(expressionTrimmed);
        else if (expressionTrimmed.matches("^\\d+(?:\\s*[*]\\s*\\d+)*$")) result = multiplication(expressionTrimmed);
        else if (expressionTrimmed.matches("^\\d+(?:\\s*[/]\\s*\\d+)*$")) result = division(expressionTrimmed);
        else result = "An expresion can only consist of numbers and a single type of operator.\n Please try again.";

        return result;
    }

    /**
     * Method used to calculate a sum expression.
     *
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public String sum(String expression) {
        String[] split = expression.split("[+]");

        return Arrays.stream(split)
                .map(Integer::parseInt)
                .reduce(0, (subtotal, element) -> subtotal + element).toString();
    }

    /**
     * Method used to calculate a subtraction expression.
     *
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public String subtraction(String expression) {
        ArrayList<String> split = Arrays.stream(expression.split("[-]")).collect(Collectors.toCollection(ArrayList::new));

        int baseNum = Integer.parseInt(split.get(0));
        split.remove(0);
        return split.stream()
                .map(Integer::parseInt)
                .reduce(baseNum, (subtotal, element) -> subtotal - element).toString();
    }

    public String multiplication(String equation) {
        String[] split = equation.split("[*]");

        return Arrays.stream(split)
                .map(Integer::parseInt)
                .reduce(1, (subtotal, element) -> subtotal * element).toString();
    }

    public String division(String equation) {
        ArrayList<String> split = Arrays.stream(equation.split("[/]")).collect(Collectors.toCollection(ArrayList::new));

        int baseNum = Integer.parseInt(split.get(0));
        split.remove(0);
        if (split.contains("0")) {
            return "Can't divide by zero";
        } else {
            return split.stream()
                    .map(Integer::parseInt)
                    .reduce(baseNum, (subtotal, element) -> subtotal / element).toString();
        }
    }
}
