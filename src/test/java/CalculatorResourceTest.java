import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals("399", calculatorResource.sum(expression));

        expression = "3+3+3+3";
        assertEquals("12", calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100-100";
        assertEquals("799", calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals("18", calculatorResource.subtraction(expression));

        expression = "20-2-4";
        assertEquals("14", calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "10*10*10";
        assertEquals("1000", calculatorResource.multiplication(equation));

        equation = "2*4*3";
        assertEquals("24", calculatorResource.multiplication(equation));

        equation = "2*12";
        assertNotEquals("200", calculatorResource.multiplication(equation));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "20/10";
        assertEquals("2", calculatorResource.division(equation));

        equation = "20/2/5";
        assertEquals("2", calculatorResource.division(equation));

        equation = "0/13";
        assertNotEquals("200", calculatorResource.division(equation));
    }
}
